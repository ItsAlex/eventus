const express = require('express');
const cors = require('cors');
const { postgraphile } = require('postgraphile');
const ConnectionFilterPlugin = require("postgraphile-plugin-connection-filter");

const port = 5454;

const app = express();

app.use(cors());

app.use(postgraphile(
    process.env.db_uri,
    'public',
    {
        watchPg: true,
        enhanceGraphiql: true,
        appendPlugins: [ConnectionFilterPlugin]
    }
));

app.listen(port, () => {
    console.info('Running on', port);
});
