const { exec } = require('node:child_process');

module.exports = async (payload) => {

    const { email, event_id } = payload;

    console.log('SENDING NEW MESSAGE TO', email);

    exec(`node email/send_email.js ${email} NEW_MESSAGE ${event_id}`, (err, output) => {
        if (err){
            console.error(err);
            return ;
        }
        console.log(output);
    });

};