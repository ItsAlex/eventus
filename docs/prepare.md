# Prepare your computer

## Package to install

### Install nodejs

```bash
brew install node
```

### Install pgcli and ansible

```bash
pip install pgcli ansible
```