# Test prod data

This documentation explain how to test production data with migrations

## Steps to reproduce

1. [Stop all local container](#1-export-production-data)
1. [Export production data](#2-export-production-data)
1. [Run up the the database](#3-run-up-the-database)
1. [Import the data](#4-import-the-data)
1. [Run up all containers](#5-run-up-all-containers)
1. [Clean your local environement](#6-clean-your-local-environment)

#### 1. Export production data

On the remote server, run the script

```bash
./scripts/dump-db.sh
```

Copy the file content of the file `./dump.sql` to your local repository
with in `./dump.sql`

All other steps will be done on your local machine

#### 2. Stop all local container

Make sur every local container are down

You can test it with

```bash
docker ps
```

#### 3. Run up the database

```bash
docker-compose -f migration.docker-compose.yml  up --build db -d
```

#### 4. Import the data

First load the secrets

```bash
source .env
```

Then use this script to load the data in your local database

```bash
./scripts/restore-db.sh
```

#### 5. Run up all containers

This will launch the graphql-api as well as the web application

```bash
docker-compose -f migration.docker-compose.yml  up --build -d
```

#### 6. Clean your local environment

When you're done, you can close the container created

```bash
docker-compose -f migration.docker-compose.yml down
```