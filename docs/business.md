# Business

Mission: Le all-in-one pour organiser un evenement

### Comment les utilisateurs nous connaissent ?

* En etant inviter a un evenement
* Par le bouche a oreille (traction initiale)
* En etant inviter sur l'application par un autre utilisateur

### Workflow d'un evenement

1. PREPARATION
    1. Sondage sur les disponibilites
    1. Sondage sur l'activites
    1. Trouver facilement un lieu
    1. Creation de l'evenement
1. CREATED
    1. Partage de l'evenement/invitation
    1. Paiement/Cagnotte
1. PLANNED
    1. Reservation
    1. Rappel de l'evenement via notifications
    1. Voir la liste des invites
1. IN_PROGRESS
    1. Realisation de l'evenement
1. DONE
    1. Note de l'evenement et commentaires
    1. Partage des photos