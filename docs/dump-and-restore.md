# Dump and restore

### Dump

To dump the content of the database `dev` you can use the script:

```
./scripts/dump-db.sh
```

### Restore

To restore, you have to be sur that the database `dev` exist, first connect to
the psql database.

```bash
psql "postgres://postgres:$POSTGRES_PASSWORD@127.0.0.1:5431/"
```

Then create the database

```sql
CREATE DATABASE dev;
```

Last but at least, you can use the script of restore.

```bash
./scripts/restore-db.sh
```