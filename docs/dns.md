# DNS

### All DNS

* [sakafet.fr](https://sakafet.fr)
* [api.sakafet.fr](https://api.sakafet.fr)
* [prom.sakafet.fr](https://prom.sakafet.fr)
* [grafana.sakafet.fr](https://grafana.sakafet.fr)

### How to add a new DNS

Most of the commands should be used with permission privilege (sudo)

1. Copy the [template](/nginx-template) and update the server_name as well as
  the port
1. Add the file to `/etc/nginx/sites-available/`
1. Create a symbolik link from the new file to `/etc/nginx/sites-enabled/`. For example `ln -s /etc/nginx/sites-available/sakafet.fr /etc/nginx/enabled/sakafet.fr`
1. Check if the config is correct with `nginx -t`
1. Restart nginx `sudo service nginx restart`
1. Run certbot to have https enabled `sudo certbot --nginx`