# ROADMAP

The next goal is to help people organizing event to do it the easiest way possible. For that, here are the list of things we must achive:

1. Groups (https://gitlab.com/S-Stanley/eventus/-/milestones/38#tab-issues)
1. Mobile application (https://gitlab.com/S-Stanley/eventus/-/milestones/3#tab-issues)
1. In-App messaging with group members (https://gitlab.com/S-Stanley/eventus/-/milestones/4#tab-issues)

Then in a second iteration:

1. Cagnotte (https://gitlab.com/S-Stanley/eventus/-/milestones/5#tab-issues)
1. Pictures (https://gitlab.com/S-Stanley/eventus/-/milestones/6#tab-issues)
