# Deployment

### Automated deployment

Just run the script:

```bash
./scripts/deployment/deploy-with-ansible.sh
```


### Manuel deployment
1. Se connecter au vps en ssh
1. Se render sur le dossier `~/sakafet/`
1. Pull main `git pull origin main`
1. Effectuer un `docker-compose up --build -d` a la racine du projet
1. Effectuer les migrations `~/scripts/prod/execute-migrations.sh`
1. Lancer le script `~/sakafet/scripts/prod/setup-seed-db-prod.sh`
