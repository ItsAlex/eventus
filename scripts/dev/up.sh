#!/bin/bash

docker-compose -f dev.docker-compose.yml up --build -d db
docker-compose -f dev.docker-compose.yml up --build -d postgraphile
docker-compose -f dev.docker-compose.yml up --build -d graphile_worker