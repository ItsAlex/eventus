#!/bin/bash

MIGRATION_FILENAME=$(date +%s)

touch "database/migrations/${MIGRATION_FILENAME}.up.sql"