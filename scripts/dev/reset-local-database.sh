#!/bin/bash

docker-compose -f dev.docker-compose.yml stop postgraphile
docker-compose exec db sh -c "cd /database/ && psql -U postgres --quiet -f fixtures.sql"
docker-compose -f dev.docker-compose.yml up -d postgraphile
