#!/bin/bash

docker-compose stop postgraphile
docker-compose exec db sh -c "cd /database/ && psql -U postgres --quiet -f prod/setup-prod.sql"
docker-compose up -d postgraphile