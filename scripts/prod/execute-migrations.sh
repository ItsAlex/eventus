#!/bin/bash

source .env

docker container prune -f
docker-compose up migrate
