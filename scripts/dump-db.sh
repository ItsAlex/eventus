#!/bin/bash

source .env
pg_dump --format=custom "postgres://postgres:$POSTGRES_PASSWORD@127.0.0.1:5431/dev" -Fp --file dump.sql