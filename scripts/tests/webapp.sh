#!/bin/bash

./scripts/dev/reset-local-database.sh   # reset local db to have a clean
                                        # one each time tests are launched

cd webapp/ && npx playwright test "$@" --workers 1