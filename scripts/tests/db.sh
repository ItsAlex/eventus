#!/bin/bash

docker-compose -f dev.docker-compose.yml down

docker-compose -f test.docker-compose.yml up --build -d postgres

docker-compose -f test.docker-compose.yml up --build --exit-code-from db-test

exit_status=$?
docker-compose -f test.docker-compose.yml down

if [[ $exit_status -eq 0 ]]; then
    exit 0;
fi
exit 1;