#!/bin/bash

source .env

psql "postgres://postgres:$POSTGRES_PASSWORD@127.0.0.1:5431/" -c "CREATE DATABASE dev"
psql "postgres://postgres:$POSTGRES_PASSWORD@127.0.0.1:5431/dev" < dump.sql
