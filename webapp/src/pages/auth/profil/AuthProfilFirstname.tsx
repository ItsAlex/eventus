import { gql, useMutation } from '@apollo/client';
import {
    Flex,
    Box,
    FormControl,
    FormLabel,
    Input,
    Stack,
    Button,
    Heading,
    useColorModeValue,
} from '@chakra-ui/react';
import { useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { toast } from 'react-toastify';
import Cookies from 'universal-cookie';

const AuthProfilFirstname = () => {

    const [firstname, setFirstname] = useState<string>("");

    const navigate = useNavigate();
    const cookies = new Cookies();
    const { user_id } = useParams();

    const [updateUser] = useMutation(gql`
        mutation updateUser(
            $firstname: String!,
            $userId: UUID!
        ) {
            updateUserById(
                input: {
                    userPatch: {
                        firstname: $firstname
                    },
                    id: $userId
                }
            ) {
                user {
                    token
                    picture
                    lastname
                    id
                    firstname
                    email
                }
            }
        }
    `)

    return (
        <Flex
            minH={'100vh'}
            align={'center'}
            justify={'center'}
            bg={useColorModeValue('gray.50', 'gray.800')}>
            <Stack spacing={8} mx={'auto'} maxW={'lg'} py={12} px={6}>
            <Stack align={'center'}>
                <Heading fontSize={'4xl'}>What is your firstname ?</Heading>
            </Stack>
            <Box
                rounded={'lg'}
                bg={useColorModeValue('white', 'gray.700')}
                boxShadow={'lg'}
                p={8}>
                <Stack spacing={4}>
                <FormControl id="email">
                    <FormLabel>Firstname</FormLabel>
                    <Input
                        data-testid='login-firstname-input'
                        type="email"
                        value={firstname}
                        onChange={(e) => setFirstname(e.target.value)}
                    />
                </FormControl>
                <Stack spacing={10}>
                    <Button
                        data-testid='login-button-submit'
                        bg={'blue.400'}
                        color={'white'}
                        _hover={{
                            bg: 'blue.500',
                        }}
                        onClick={async() => {
                            try {
                                if (!firstname) {
                                    toast.error('Your firstname cannot be empty');
                                    return ;
                                }
                                const req = await updateUser({
                                    variables: {
                                        firstname: firstname,
                                        userId: user_id
                                    }
                                })
                                cookies.set('user_id', req?.data?.updateUserById?.user?.id);
                                cookies.set('email', req?.data?.updateUserById?.user?.email);
                                cookies.set('token', req?.data?.updateUserById?.user?.token);
                                cookies.set('firstname', req?.data?.updateUserById?.user?.firstname);
                                cookies.set('lastname', req?.data?.updateUserById?.user?.lastname);
                                toast.success('You are now sigin');
                                navigate('/events/');
                            } catch (e) {
                                toast.error('Error, cannot login');
                            }
                        }}
                    >
                        Validate
                    </Button>
                </Stack>
                </Stack>
            </Box>
            </Stack>
        </Flex>
    );
}

export default AuthProfilFirstname;