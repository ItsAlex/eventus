/* eslint-disable testing-library/prefer-screen-queries */

import { test, expect } from '@playwright/test';

test('Assert that user can login', async({page}) => {
    await page.goto('http://localhost:3000/auth/register/');
    await page.getByTestId('register-email-input').fill('new-account@sakafet.fr');
    await page.getByTestId('register-firstname-input').fill('Myfirstname');
    await page.getByTestId('register-password-input').fill('secret-password');
    await page.getByTestId('register-button-submit').click();

    await expect(page).toHaveURL('http://localhost:3000/events/');
});
