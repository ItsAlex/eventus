/* eslint-disable testing-library/prefer-screen-queries */
import { test, expect } from '@playwright/test';
const { Client } = require('pg');

require('dotenv').config();

const client = new Client({
    port: 5431,
    password: process.env.POSTGRES_PASSWORD,
    user: 'postgres',
    database: 'dev',
});

const connectUser = async(page) => {
    await page.goto('http://localhost:3000/auth/login/');
    await page.getByTestId('login-email-input').fill('user0001@sakafet.fr');
    await page.getByTestId('login-password-input').fill('test');
    await page.getByTestId('login-button-submit').click();

    await expect(page).toHaveURL('http://localhost:3000/events/');
}

test('Should create a new group', async({page}) => {
    await client.connect();
    await connectUser(page);

    await page.getByTestId('group-list-page').click();

    await page.getByTestId('create-new-group-button').click();
    await expect(page).toHaveURL('http://localhost:3000/groups/new/');

    await page.getByTestId('new-group-input-name').fill('test group');
    await page.selectOption('#new-group-select-activity', 'd25b7e5b-8b23-490b-9140-55c766cd0003');
    await page.selectOption('#new-group-select-frequency', 'EVERY_WEEK');

    await page.getByTestId('new-group-submit-button').click();

    await expect(page).toHaveURL('http://localhost:3000/groups/');

    const group = await client.query(
        "SELECT name, activity_id, frequency, created_by FROM public.groups WHERE id=(SELECT id FROM public.groups WHERE activity_id=$1 AND frequency=$2 ORDER BY created_at DESC LIMIT 1)",
        ['d25b7e5b-8b23-490b-9140-55c766cd0003', 'EVERY_WEEK']
    );
    expect(group?.rows[0]).toEqual({
        name: 'test group',
        activity_id: 'd25b7e5b-8b23-490b-9140-55c766cd0003',
        frequency: 'EVERY_WEEK',
        created_by: '8e91753e-8c33-44fd-ada9-f4a2a62330ec'
    });
    await client.query(
        "DELETE FROM public.groups WHERE id=(SELECT id FROM public.groups WHERE activity_id=$1 AND frequency=$2 ORDER BY created_at DESC LIMIT 1);",
        ['d25b7e5b-8b23-490b-9140-55c766cd0003', 'EVERY_WEEK']
    );
    await client.end();
});
