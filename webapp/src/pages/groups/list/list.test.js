/* eslint-disable testing-library/prefer-screen-queries */
import { test, expect } from '@playwright/test';

const connectUser = async(page) => {
    await page.goto('http://localhost:3000/auth/login/');
    await page.getByTestId('login-email-input').fill('user0001@sakafet.fr');
    await page.getByTestId('login-password-input').fill('test');
    await page.getByTestId('login-button-submit').click();

    await expect(page).toHaveURL('http://localhost:3000/events/');
}

test('Click on new group button should have the right redirection', async({page}) => {
    await connectUser(page);

    await page.getByTestId('group-list-page').click();

    await page.getByTestId('create-new-group-button').click();
    await expect(page).toHaveURL('http://localhost:3000/groups/new/');
});

test('Should display group list page', async({ page }) => {
    await connectUser(page);

    await page.getByTestId('group-list-page').click();
    await expect(page.getByTestId('group-list-table')).toHaveScreenshot();
});