const UserIsNotLogged = () => {
    return (
        <div style={{ padding: '2rem' }}>
            You have to be logged to access this page
        </div>
    )
}

export default UserIsNotLogged;
