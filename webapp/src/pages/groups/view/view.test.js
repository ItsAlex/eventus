/* eslint-disable testing-library/prefer-screen-queries */
import { test, expect } from '@playwright/test';

const connectUser = async(page) => {
    await page.goto('http://localhost:3000/auth/login/');
    await page.getByTestId('login-email-input').fill('user0001@sakafet.fr');
    await page.getByTestId('login-password-input').fill('test');
    await page.getByTestId('login-button-submit').click();

    await expect(page).toHaveURL('http://localhost:3000/events/');
}

test('Should access group setting page', async({page}) => {
    await connectUser(page);

    await page.getByTestId('group-list-page').click();
    await page.getByTestId('groups-03e94668-0deb-47fa-b4dc-b44724eb0001').click();
    await page.getByTestId('settings-group-button').click();

    expect(page.url()).toBe('http://localhost:3000/groups/03e94668-0deb-47fa-b4dc-b44724eb0001/settings/');
});