import { Button, Input, Text } from "@chakra-ui/react";
import { useNavigate, useParams } from "react-router-dom";
import { toast } from "react-toastify";

const SettingGroupPage = () => {
    const navigate = useNavigate();
    const { group_id } = useParams();

    return (
        <div>
            <div style={{ padding: '2rem' }}>
                <Text>Copy this link and send it to invite your friends to this group</Text>
                <div style={{ display: 'flex' }}>
                    <Input
                        type="text"
                        data-testid='group-invitation-link-input'
                        defaultValue={window.location.href.replace('settings/', '')}
                    />
                    <Button
                        onClick={() => {
                            try {
                                navigator.clipboard.writeText(window.location.href);
                                toast.success('URL copied');
                            } catch (e) {
                                console.error(e);
                                toast.error('Cannot copy URL, please do it manually');
                            }
                        }}
                    >
                        Copy
                    </Button>
                </div>
            </div>
            <hr/>
            <div style={{ padding: '2rem' }}>
                <div
                    style={{
                        textDecoration: 'underline',
                        cursor: 'pointer'
                    }}
                    onClick={() => navigate(`/groups/${group_id}/settings/members`)}
                    data-testid='members-list-button'
                >
                    See members list
                </div>
            </div>
        </div>
    );
};

export default SettingGroupPage;