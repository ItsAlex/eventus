/* eslint-disable testing-library/prefer-screen-queries */
import { test, expect } from '@playwright/test';
const { Client } = require('pg');

require('dotenv').config();

const client = new Client({
    port: 5431,
    password: process.env.POSTGRES_PASSWORD,
    user: 'postgres',
    database: 'dev',
});

test('User can confirm a event previously in DRAFT', async({page}) => {
    await page.goto('http://localhost:3000/auth/login/');
    await page.getByTestId('login-email-input').fill('user0001@sakafet.fr');
    await page.getByTestId('login-password-input').fill('test');
    await page.getByTestId('login-button-submit').click();
    await expect(page).toHaveURL('http://localhost:3000/events/');

    await page.getByTestId('button-view-event-1').click();
    await page.getByTestId('event-settings-icon').click();
    await page.getByTestId('confirm-event').click();

    await expect(await page.getByTestId('event-settings-status').innerText()).toEqual('DRAFT');

    await client.connect();

    const event_status = await client.query("SELECT status FROM public.events WHERE id='376ddd49-ffef-45c6-aadb-7a234f3be003'");
    expect(event_status?.rows.length).toEqual(1);
    expect(event_status.rows[0].status).toEqual('CONFIRMED');

    await client.query("UPDATE public.events SET status='DRAFT' where id='376ddd49-ffef-45c6-aadb-7a234f3be003';");

    await client.end();
});
