/* eslint-disable testing-library/prefer-screen-queries */
import { test, expect } from '@playwright/test';
const { Client } = require('pg');

require('dotenv').config();

const client = new Client({
    port: 5431,
    password: process.env.POSTGRES_PASSWORD,
    user: 'postgres',
    database: 'dev',
});

test('User can create a survey', async({page}) => {
    await client.connect();

    await page.goto('http://localhost:3000/auth/login/');
    await page.getByTestId('login-email-input').fill('user0001@sakafet.fr');
    await page.getByTestId('login-password-input').fill('test');
    await page.getByTestId('login-button-submit').click();

    await expect(page).toHaveURL('http://localhost:3000/events/');

    await page.getByTestId('my-events-navbar').click();
    await page.getByTestId('button-view-event-0').click();

    await page.getByTestId('event-settings-icon').click();
    await page.getByTestId('survey-event').click();
    await page.getByTestId('new-survey-button').click();

    await page.selectOption('#new-survey-timeline', 'SEVEN_DAYS');

    await page.getByTestId('submit-button-new-survey').click();
    await page.waitForTimeout(1000);

    const survey = await client.query("SELECT id, event_id FROM public.survey WHERE event_id='376ddd49-ffef-45c6-aadb-7a234f3be001' ORDER BY created_at DESC");
    expect(page).toHaveURL(`http://localhost:3000/events/${survey.rows[0].event_id}/settings/survey/${survey.rows[0].id}`);

    await client.query("DELETE FROM public.survey WHERE id=(SELECT id FROM public.survey WHERE event_id='376ddd49-ffef-45c6-aadb-7a234f3be001' ORDER BY created_at DESC LIMIT 1);");
    await client.end();
});
