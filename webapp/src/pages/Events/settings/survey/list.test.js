/* eslint-disable testing-library/prefer-screen-queries */
import { test, expect } from '@playwright/test';
const { Client } = require('pg');

require('dotenv').config();

const client = new Client({
    port: 5431,
    password: process.env.POSTGRES_PASSWORD,
    user: 'postgres',
    database: 'dev',
});

const connectUser = async(page) => {
    await page.goto('http://localhost:3000/auth/login/');
    await page.getByTestId('login-email-input').fill('user0001@sakafet.fr');
    await page.getByTestId('login-password-input').fill('test');
    await page.getByTestId('login-button-submit').click();

    await expect(page).toHaveURL('http://localhost:3000/events/');
}

const accessToEventSettingPage = async(page) => {
    await page.getByTestId('my-events-navbar').click();
    await page.getByTestId('button-view-event-0').click();
    await page.getByTestId('event-settings-icon').click();
};


test('User can see list of surveys', async({page}) => {
    await client.connect();

    await connectUser(page);
    await accessToEventSettingPage(page);

    await page.getByTestId('survey-event').click();

    await page.waitForTimeout(1000);
    await expect(page.getByTestId('survey-event-list')).toHaveScreenshot();
});
