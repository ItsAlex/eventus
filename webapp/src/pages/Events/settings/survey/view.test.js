/* eslint-disable testing-library/prefer-screen-queries */
import { test, expect } from '@playwright/test';

const connectUser = async(page) => {
    await page.goto('http://localhost:3000/auth/login/');
    await page.getByTestId('login-email-input').fill('user0001@sakafet.fr');
    await page.getByTestId('login-password-input').fill('test');
    await page.getByTestId('login-button-submit').click();

    await expect(page).toHaveURL('http://localhost:3000/events/');
}

const accessToEventSettingPage = async(page) => {
    await page.getByTestId('my-events-navbar').click();
    await page.getByTestId('button-view-event-0').click();
    await page.getByTestId('event-settings-icon').click();
};

test('User can view result of survey', async({page}) => {
    await connectUser(page);
    await accessToEventSettingPage(page);


    await page.getByTestId('survey-event').click();
    await page.getByTestId('button-survey-view-page-0').click();
    await page.waitForTimeout(1000);

    await expect(page).toHaveURL('http://localhost:3000/events/376ddd49-ffef-45c6-aadb-7a234f3be001/settings/survey/357f4f0b-b0c8-4f1e-b453-5e8b30240001');

    await expect(page.getByTestId('table-list-result-survey')).toHaveScreenshot();

});
