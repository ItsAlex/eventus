/* eslint-disable testing-library/prefer-screen-queries */
import { test, expect } from '@playwright/test';

const connectUser = async(page) => {
    await page.goto('http://localhost:3000/auth/login/');
    await page.getByTestId('login-email-input').fill('user0001@sakafet.fr');
    await page.getByTestId('login-password-input').fill('test');
    await page.getByTestId('login-button-submit').click();

    await expect(page).toHaveURL('http://localhost:3000/events/');
}

const accessToEventSettingPage = async(page, event_number = 0) => {
    await page.getByTestId('my-events-navbar').click();
    await page.getByTestId(`button-view-event-${event_number}`).click();
    await page.getByTestId('event-settings-icon').click();
};

test('User can access the founding view page', async({ page }) => {
    await connectUser(page);
    await accessToEventSettingPage(page);

    await page.getByTestId('event-founding-button').click();
    await expect(page).toHaveURL('http://localhost:3000/events/376ddd49-ffef-45c6-aadb-7a234f3be001/settings/founding/');
});

test('Deactivate, Activate, Redeactivate and Reactivate founding', async ({ page }) => {
    await connectUser(page);
    await accessToEventSettingPage(page, 2);

    await page.getByTestId('event-founding-button').click();
    await page.getByTestId('deactivate-founding-event').click();
    await expect(page).toHaveURL('http://localhost:3000/events/376ddd49-ffef-45c6-aadb-7a234f3be003/settings/');

    await page.getByTestId('event-founding-button').click();
    await page.getByTestId('activate-founding-event').click();
    await expect(page).toHaveURL('http://localhost:3000/events/376ddd49-ffef-45c6-aadb-7a234f3be003/settings/');

    await page.getByTestId('event-founding-button').click();
    await page.getByTestId('deactivate-founding-event').click();
    await expect(page).toHaveURL('http://localhost:3000/events/376ddd49-ffef-45c6-aadb-7a234f3be003/settings/');

    await page.getByTestId('event-founding-button').click();
    await page.getByTestId('activate-founding-event').click();
    await expect(page).toHaveURL('http://localhost:3000/events/376ddd49-ffef-45c6-aadb-7a234f3be003/settings/');
});

test('User cannot activate founding if there is already one participant to the event', async ({ page }) => {
    await connectUser(page);
    await accessToEventSettingPage(page, 0);

    await page.getByTestId('event-founding-button').click();
    await page.getByTestId('input-price-ticket').fill('2');
    await page.getByTestId('activate-founding-event').click();
    await expect(page).toHaveURL('http://localhost:3000/events/376ddd49-ffef-45c6-aadb-7a234f3be001/settings/founding/');
});