/* eslint-disable testing-library/prefer-screen-queries */

import { test, expect } from '@playwright/test';
const { Client } = require('pg');

require('dotenv').config();

const client = new Client({
    port: 5431,
    password: process.env.POSTGRES_PASSWORD,
    user: 'postgres',
    database: 'dev',
});

test('User can create event', async({page}) => {
    await client.connect();

    await page.goto('http://localhost:3000/auth/login/');
    await page.getByTestId('login-email-input').fill('user0001@sakafet.fr');
    await page.getByTestId('login-password-input').fill('test');
    await page.getByTestId('login-button-submit').click();
    await expect(page).toHaveURL('http://localhost:3000/events/');

    await page.getByTestId('button-new-event').click()
    await expect(page).toHaveURL('http://localhost:3000/events/new/');

    await page.getByTestId('input-event-name').fill('event test');
    await page.getByTestId('submit-button-new-event').click();

    await page.waitForTimeout(1000);
    await expect(await page.getByTestId('event-view-name').innerText()).toEqual('event test');

    await client.query("DELETE FROM public.events WHERE name='event test'");

    await client.end();
});