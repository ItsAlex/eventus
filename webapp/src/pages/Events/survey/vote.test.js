/* eslint-disable testing-library/prefer-screen-queries */
import { test, expect } from '@playwright/test';
const { Client } = require('pg');

require('dotenv').config();

const client = new Client({
    port: 5431,
    password: process.env.POSTGRES_PASSWORD,
    user: 'postgres',
    database: 'dev',
});

const connectUser = async(page) => {
    await page.goto('http://localhost:3000/auth/login/');
    await page.getByTestId('login-email-input').fill('user0001@sakafet.fr');
    await page.getByTestId('login-password-input').fill('test');
    await page.getByTestId('login-button-submit').click();

    await expect(page).toHaveURL('http://localhost:3000/events/');
}

const accessToEventSettingPage = async(page) => {
    await page.getByTestId('my-events-navbar').click();
    await page.getByTestId('button-view-event-0').click();
    await page.getByTestId('event-settings-icon').click();
};


test('User can vote in a survey', async({page}) => {
    await client.connect();

    await connectUser(page);
    await accessToEventSettingPage(page);

    await page.getByTestId('survey-event').click();
    await page.getByTestId('new-survey-button').click();

    await page.selectOption('#new-survey-timeline', 'SEVEN_DAYS');

    await page.getByTestId('submit-button-new-survey').click();
    await page.waitForTimeout(1000);

    const survey = await client.query("SELECT id, event_id FROM public.survey WHERE event_id='376ddd49-ffef-45c6-aadb-7a234f3be001' ORDER BY created_at DESC");
    expect(page).toHaveURL(`http://localhost:3000/events/${survey.rows[0].event_id}/settings/survey/${survey.rows[0].id}`);


    await page.getByTestId('survey-button-vote').click();
    await page.waitForTimeout(1000);


    await page.getByTestId('checkbox-vote-0').click();
    await page.getByTestId('checkbox-vote-3').click();

    await page.getByTestId('button-save-survey').click();
    await page.waitForTimeout(1000);

    expect(page).toHaveURL(`http://localhost:3000/events/${survey.rows[0].event_id}/`);

    const survey_answer = await client.query("SELECT id, answer FROM public.survey_answers WHERE survey_id=$1 ORDER BY created_at DESC", [survey.rows[0].id]);

    expect(survey_answer.rows[0].answer).toEqual('true,false,false,true,false,false,false');

    await client.query('DELETE FROM public.survey_answers WHERE survey_id=$1;', [survey.rows[0].id]);
    await client.query("DELETE FROM public.survey WHERE id=$1;", [survey.rows[0].id]);
});

test('User can update vote for survey', async({page}) => {

    await connectUser(page);
    await accessToEventSettingPage(page);

    await page.getByTestId('survey-event').click();
    await page.getByTestId('button-survey-view-page-0').click();
    await page.waitForTimeout(1000);

    await expect(page).toHaveURL('http://localhost:3000/events/376ddd49-ffef-45c6-aadb-7a234f3be001/settings/survey/357f4f0b-b0c8-4f1e-b453-5e8b30240001');


    await page.getByTestId('survey-button-vote').click();
    await page.waitForTimeout(1000);


    await page.getByTestId('checkbox-vote-2').click();

    await page.getByTestId('button-save-survey').click();
    await page.waitForTimeout(1000);

    const survey = await client.query("SELECT id, event_id FROM public.survey WHERE event_id='376ddd49-ffef-45c6-aadb-7a234f3be001' ORDER BY created_at DESC");
    expect(page).toHaveURL(`http://localhost:3000/events/${survey.rows[0].event_id}/`);

    const survey_answer = await client.query("SELECT id, answer FROM public.survey_answers WHERE survey_id=$1 AND user_id=$2 ORDER BY created_at DESC", [survey.rows[0].id, '8e91753e-8c33-44fd-ada9-f4a2a62330ec']);

    expect(survey_answer.rows[0].answer).toEqual('true,true,true,false,false,false,false');

    await client.query('UPDATE public.survey_answers SET answer=$1 WHERE survey_id=$2 AND user_id=$3;', ['true,true,false,false,false,false,false', survey.rows[0].id, '8e91753e-8c33-44fd-ada9-f4a2a62330ec']);
    await client.end();
});
