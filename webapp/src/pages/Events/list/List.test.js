/* eslint-disable testing-library/prefer-screen-queries */
import { test, expect } from '@playwright/test';

require('dotenv').config();

test('User can visualise list of events', async({page}) => {
    await page.goto('http://localhost:3000/auth/login/');
    await page.getByTestId('login-email-input').fill('user0001@sakafet.fr');
    await page.getByTestId('login-password-input').fill('test');
    await page.getByTestId('login-button-submit').click();
    await expect(page).toHaveURL('http://localhost:3000/events/');

    await page.waitForTimeout(1000);
    await expect(page.getByTestId('event-list')).toHaveScreenshot();
});