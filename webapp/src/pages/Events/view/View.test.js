/* eslint-disable testing-library/prefer-screen-queries */

import { test, expect } from '@playwright/test';

const connectUser = async(page) => {
    await page.goto('http://localhost:3000/auth/login/');
    await page.getByTestId('login-email-input').fill('user0001@sakafet.fr');
    await page.getByTestId('login-password-input').fill('test');
    await page.getByTestId('login-button-submit').click();

    await expect(page).toHaveURL('http://localhost:3000/events/');
}

const accessToEventViewPage = async(page, event_number = 0) => {
    await page.getByTestId('my-events-navbar').click();
    await page.getByTestId(`button-view-event-${event_number}`).click();
};


test('User can view event', async({page}) => {
    await connectUser(page);

    await page.goto('http://localhost:3000/events/376ddd49-ffef-45c6-aadb-7a234f3be002/')

    await expect(await page.getByTestId('event-view-address').innerText()).toEqual('10 avenue Aubervillier');
    await expect(await page.getByTestId('event-view-price').innerText()).toEqual('10.50€');
    await expect(await page.getByTestId('event-view-status').innerText()).toEqual('CONFIRMED');
});


test('User can see the price of the event',  async ({ page }) => {
    await connectUser(page);
    await accessToEventViewPage(page, '2');

    await expect(await page.getByTestId('button-participation-event').textContent()).toBe('Save (7.50€)');
});