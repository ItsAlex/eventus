import {
    Box,
    Flex,
    Text,
    IconButton,
    Button,
    Stack,
    Collapse,
    Icon,
    Link,
    Popover,
    PopoverTrigger,
    useColorModeValue,
    useBreakpointValue,
    useDisclosure,
} from '@chakra-ui/react';
  import {
    HamburgerIcon,
    CloseIcon,
    ChevronDownIcon,
} from '@chakra-ui/icons';
import { useNavigate } from 'react-router-dom';
import { useEffect, useState } from 'react';
import { gql, useMutation } from '@apollo/client';
import Cookies from 'universal-cookie';

const menu: NavItem[] = [
    {
        href: '/events/',
        label: 'All events',
        id: 'all-event-navbar'
    },
    {
        href: '/users/{USER_ID}/events/',
        label: 'My events',
        id: 'my-events-navbar'
    },
    {
        href: '/events/new/',
        label: 'Create new event',
        id: 'new-events-navbar'
    },
    {
        href: '/messagerie/',
        label: 'Messagerie',
        id: 'messagerie-navbar'
    },
    {
        href: '/groups/',
        label: 'Groups',
        id: 'group-list-page'
    }
]

const Navbar = () => {

    const { isOpen, onToggle } = useDisclosure();
    const navigate = useNavigate();
    const cookies = new Cookies();
    const [isuserLogged, setIsUserLogged] = useState<boolean>(false);

    const [checkToken] = useMutation(gql`
        mutation checkToken (
            $userToken: UUID
        ) {
            checkToken(
                input: {
                    appUserToken: $userToken
                }
            ) {
                boolean
            }
        }
    `)

    const check_user_logged = async() => {
        try {
            await checkToken({
                variables: {
                    userToken: cookies.get('token')
                }
            });
            setIsUserLogged(true);
        } catch (e) {
            console.error(e);
        }
    };

    useEffect(() => {
        check_user_logged();
    }, [window.location.href]);

    return (
        <Box>
            <Flex
                bg={useColorModeValue('white', 'gray.800')}
                color={useColorModeValue('gray.600', 'white')}
                minH={'60px'}
                py={{ base: 2 }}
                px={{ base: 4 }}
                borderBottom={1}
                borderStyle={'solid'}
                borderColor={useColorModeValue('gray.200', 'gray.900')}
                align={'center'}
            >
                <Flex
                    flex={{ base: 1, md: 'auto' }}
                    ml={{ base: -2 }}
                    display={{ base: 'flex', md: 'none' }}
                >
                    <IconButton
                        onClick={onToggle}
                        icon={
                            isOpen ? <CloseIcon w={3} h={3} /> : <HamburgerIcon w={5} h={5} />
                        }
                        variant={'ghost'}
                        aria-label={'Toggle Navigation'}
                    />
                </Flex>
                <Flex flex={{ base: 1 }} justify={{ base: 'center', md: 'start' }}>
                    <Text
                        textAlign={useBreakpointValue({ base: 'center', md: 'left' })}
                        fontFamily={'heading'}
                        color={useColorModeValue('gray.800', 'white')}
                        onClick={() => navigate('/')}
                        style={{
                            cursor: 'pointer'
                        }}
                    >
                        Sakafet.fr
                    </Text>
                    {isuserLogged &&
                        <Flex display={{ base: 'none', md: 'flex' }} ml={10}>
                            <DesktopNav />
                        </Flex>
                    }
                </Flex>

                {isuserLogged ? (
                    <div>
                        Connected as {cookies.get('email')}
                        &nbsp;
                        <span
                            style={{ textDecoration: 'underline' }}
                            onClick={() => {
                                Object.keys(cookies.getAll()).forEach((cookieName) => {
                                    console.log(cookieName);
                                    cookies.remove(cookieName, {
                                        path: '/'
                                    });
                                });
                                navigate('/');
                            }}
                        >
                            Disconnect
                        </span>
                    </div>
                ) : (
                    <Stack
                        flex={{ base: 1, md: 0 }}
                        justify={'flex-end'}
                        direction={'row'}
                        spacing={6}
                    >
                        <Button
                            as={'a'}
                            fontSize={'sm'}
                            fontWeight={400}
                            variant={'link'}
                            onClick={() => navigate('/auth/login/')}
                        >
                            Sign In
                        </Button>
                        <Button
                            as={'a'}
                            display={{ base: 'none', md: 'inline-flex' }}
                            fontSize={'sm'}
                            fontWeight={600}
                            color={'white'}
                            bg={'pink.400'}
                            onClick={() => navigate('/auth/register/')}
                            _hover={{
                                bg: 'pink.300',
                            }}
                        >
                            Sign Up
                        </Button>
                    </Stack>
                )}
            </Flex>

            <Collapse in={isOpen} animateOpacity>
                <MobileNav />
            </Collapse>
        </Box>
    );
}

const DesktopNav = () => {
    const navigate = useNavigate();
    const cookies = new Cookies();

    return (
        <Stack direction={'row'} spacing={4}>
            {menu.map((navItem: NavItem) => (
                <Box key={navItem.label}>
                <Popover trigger={'hover'} placement={'bottom-start'}>
                    <PopoverTrigger>
                        <div
                            onClick={async() => navigate(navItem.href.replaceAll('{USER_ID}', await cookies.get('user_id')))}
                            style={{
                                cursor: 'pointer'
                            }}
                            data-testid={navItem.id}
                        >
                            {navItem.label}
                        </div>
                    </PopoverTrigger>
                </Popover>
                </Box>
            ))}
        </Stack>
    );
};

const MobileNav = () => {
    return (
        <Stack
        bg={useColorModeValue('white', 'gray.800')}
        p={4}
        display={{ md: 'none' }}>
        {menu.map((navItem) => (
            <MobileNavItem key={navItem.label} {...navItem} />
        ))}
        </Stack>
    );
};

const MobileNavItem = ({ label, children, href }: NavItem) => {
    const { isOpen, onToggle } = useDisclosure();
    const navigate = useNavigate();
    const cookies = new Cookies();

    return (
        <Stack spacing={4} onClick={children && onToggle}>
        <Flex
            py={2}
            as={Link}
            onClick={async() => navigate(href.replaceAll('{USER_ID}', await cookies.get('user_id')))}
            justify={'space-between'}
            align={'center'}
            _hover={{
                textDecoration: 'none',
            }}
        >
            <Text
                fontWeight={600}
                color={useColorModeValue('gray.600', 'gray.200')}
            >
                {label}
            </Text>
            {children && (
                <Icon
                    as={ChevronDownIcon}
                    transition={'all .25s ease-in-out'}
                    transform={isOpen ? 'rotate(180deg)' : ''}
                    w={6}
                    h={6}
                />
            )}
        </Flex>
        </Stack>
    );
};

interface NavItem {
    label: string;
    subLabel?: string;
    children?: Array<NavItem>;
    href: string;
    id: string;
}

export default Navbar;
