export enum GroupFrequencyEnum {
    'EVERY_WEEK' = 'EVERY_WEEK',
    'EVERY_TWO_WEEKS' = 'EVERY_TWO_WEEKS'
};