export enum FoundingStatusEnum {
    'ACTIVATED' = 'ACTIVATED',
    'DEACTIVATED' = 'DEACTIVATED'
};