export enum surveyTimelineEnum {
    'SEVEN_DAYS' = 'SEVEN_DAYS',
    'FOURTEEN_DAYS' = 'FOURTEEN_DAYS',
    'THIRTY_DAYS' = 'THIRTY_DAYS'
}