import {IUser} from "./IUser";

export interface IMessage {
    id: string,
    userId: string,
    content: string,
    createdAt: string,
    userByUserId: IUser
}
