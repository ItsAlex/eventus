# Sakafet

### Purpose

The goal of this project is to allow to create event easily (surveys,
invitations, messagerie, groups, recurring events, etc).

[sakafet.fr](https://sakafet.fr)

### Setting up the project

Before the project can be started locally, you will need to make sur to
have all this tools installed on your computer:

* NodeJS
* Docker
* Docker-compose

### How to start the project locally

1. Run the script `./scripts/dev/up.sh` to run all docker container
1. Then setup all the databases seeds with `./scripts/dev/reset-local-database.sh`
1. Go to the webapp folder `cd /webapp`
1. Run `npm i` to install the dependencies
1. Run the front-end with `npm start`

### URL

* postgrahile: [http://localhost:5454/graphiql](http://localhost:5454/graphiql)
* webapp: [http://localhost:4242/](http://localhost:4242/)
