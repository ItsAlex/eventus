const { gql, GraphQLClient } = require('graphql-request');
const os = require('os');
const { differenceInCalendarDays } = require('date-fns');
const { exec } = require('node:child_process');

const client = new GraphQLClient(
    os.hostname() === 'MacBook-Pro-de-Stanley.local'
    ? 'http://localhost:5454/graphql'
    : 'https://api.sakafet.fr/graphql'
);

const query = gql`
    query MyQuery (
        $today: Datetime
    ) {
        allEvents (
            filter: {
                dateOfEvent: {
                    greaterThan: $today
                }
            }
        ) {
            nodes {
                id
                dateOfEvent
                eventParticipationsByEventId {
                    nodes {
                        userByUserId {
                            email
                        }
                    }
                }
            }
        }
    }
`

const notification_3_days = async() => {
    const data = await client.request(query, {
        today: new Date()
    });
    (data?.allEvents?.nodes ?? []).forEach(async(node) => {
        if (
            differenceInCalendarDays(new Date(node?.dateOfEvent), new Date()) < 3
            &&
            differenceInCalendarDays(new Date(node?.dateOfEvent), new Date()) > 1
        ){
            (node?.eventParticipationsByEventId?.nodes ?? []).map((participant) => {
                exec(`node email/send_email.js ${participant?.userByUserId?.email} RAPPEL_EVENT_3_DAYS ${node?.id}`, (err, output) => {
                    if (err){
                        console.error(err);
                        return ;
                    }
                    console.log(output);
                });
            });
        }
    });
}

const notification_1_days = async() => {
    const data = await client.request(query, {
        today: new Date()
    });
    (data?.allEvents?.nodes ?? []).forEach(async(node) => {
        if (
            differenceInCalendarDays(new Date(node?.dateOfEvent), new Date()) < 1
        ){
            (node?.eventParticipationsByEventId?.nodes ?? []).map((participant) => {
                exec(`node email/index.js ${participant?.userByUserId?.email} RAPPEL_EVENT_1_DAY ${node?.id}`, (err, output) => {
                    if (err){
                        console.error(err);
                        return ;
                    }
                    console.log(output);
                });
            });
        }
    });
}

const notification_1_hour = async() => {
    const data = await client.request(query, {
        today: new Date()
    });
    (data?.allEvents?.nodes ?? []).forEach(async(node) => {
        if (
            differenceInCalendarDays(new Date(node?.dateOfEvent), new Date()) < 3
        ){
            (node?.eventParticipationsByEventId?.nodes ?? []).map((participant) => {
                exec(`node email/index.js ${participant?.userByUserId?.email} RAPPEL_EVENT_1_HOUR ${node?.id}`, (err, output) => {
                    if (err){
                        console.error(err);
                        return ;
                    }
                    console.log(output);
                });
            });
        }
    });
}

notification_3_days();
notification_1_days();
notification_1_hour();