const nodemailer = require("nodemailer");
const fs = require('fs');
require('dotenv').config();
const { gql, GraphQLClient } = require('graphql-request');
const os = require('os');

if (process.argv.length <= 2){
    console.log('missing arguments');
    process.exit(1);
}

const destinataire = process.argv[2];
const template_name = process.argv[3];
const event_id = process.argv[4];


const subject = {
    RAPPEL_EVENT_3_DAYS: 'Your event start in 3 days',
    RAPPEL_EVENT_1_DAY: 'Your event start in 1 day',
    RAPPEL_EVENT_1_HOUR: 'Your event start in 1 hour',
    NEW_EVENT: 'A new event may interest you',
    NEW_MESSAGE: 'A new message has been posted'
}

const client = new GraphQLClient(
    os.hostname() === 'MacBook-Pro-de-Stanley.local'
    ? 'http://localhost:5454/graphql'
    : 'https://api.sakafet.fr/graphql'
);

const query = gql`
    query getEvent($id: UUID!) {
        eventById(id: $id) {
            name
            id
            address
            dateOfEvent
        }
    }
`

async function main() {

    const data = await client.request(query, {
        id: event_id
    })

    const template = fs.readFileSync(`./email/templates/${template_name}.html`, {
        encoding:'utf8',
        flag:'r'
    })
        .replaceAll('{{name}}', data.eventById?.name)
        .replaceAll('{{event_id}}', data.eventById?.id)
        .replaceAll('{{url}}', os.hostname() === 'vps-7c17b235' ? 'https://sakafet.fr' : `http://localhost:3000`)
        .replaceAll('{{address}}', data.eventById?.address)
        .replaceAll('{{dateOfEvent}}', new Date(data?.eventById?.dateOfEvent).toLocaleString())
    ;

    let transporter = nodemailer.createTransport({
        host: 'ssl0.ovh.net',
        port: 465,
        secure: true,
        auth: {
            user: process.env.SMTP_EMAIL,
            pass: process.env.SMTP_PASSWORD,
        },
    });

    let info = await transporter.sendMail({
      from: process.env.SMTP_EMAIL,
      to: destinataire,
      subject: subject[template_name],
      text: template,
      html: template,
    });

    console.log("Message sent: %s", info.messageId);

    console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
}

main().catch((e) => {
    console.error(e);
    process.exit(1);
});
