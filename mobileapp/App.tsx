/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 */

import React from 'react';
import { SafeAreaView, ScrollView, Text, View } from 'react-native';

function App(): JSX.Element {
    return (
        <SafeAreaView>
            <ScrollView>
                <View>
                    <Text>Sakafet mobile app</Text>
                </View>
            </ScrollView>
        </SafeAreaView>
    );
}
export default App;
