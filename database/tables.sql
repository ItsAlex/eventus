CREATE TABLE public.users (
    id UUID UNIQUE NOT NULL DEFAULT uuid_generate_v4(),
    email VARCHAR(200) UNIQUE NOT NULL,
    firstname VARCHAR(200) DEFAULT NULL,
    lastname VARCHAR(200) DEFAULT NULL,
    token UUID UNIQUE NOT NULL DEFAULT uuid_generate_v4(),
    picture VARCHAR(250) DEFAULT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT NOW(),
    password TEXT DEFAULT NULL
);

CREATE TABLE public.login (
    id UUID UNIQUE NOT NULL DEFAULT uuid_generate_v4(),
    user_id UUID NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT NOW()
);

CREATE TABLE public.activities(
    id UUID UNIQUE NOT NULL DEFAULT uuid_generate_v4(),
    name VARCHAR(200) UNIQUE NOT NULL
);

CREATE TYPE public.group_frequency AS ENUM (
    'EVERY_WEEK',
    'EVERY_TWO_WEEKS'
);

CREATE TABLE public.groups (
    id UUID UNIQUE NOT NULL DEFAULT uuid_generate_v4(),
    name VARCHAR(250) NOT NULL,
    activity_id UUID NOT NULL,
    frequency public.group_frequency NOT NULL,
    created_by UUID NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT NOW(),

    CONSTRAINT fk_activity_id FOREIGN KEY (activity_id) REFERENCES public.activities(id),
    CONSTRAINT fk_created_by FOREIGN KEY (created_by) REFERENCES public.users(id)
);

CREATE TABLE public.group_members (
    id UUID UNIQUE NOT NULL DEFAULT uuid_generate_v4(),
    group_id UUID NOT NULL,
    user_id UUID NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT NOW(),

    CONSTRAINT fk_group_id FOREIGN KEY (group_id) REFERENCES public.groups(id) ON DELETE CASCADE,
    CONSTRAINT fk_user_id FOREIGN KEY (user_id) REFERENCES public.users(id) ON DELETE CASCADE
);


CREATE TYPE public.event_status as ENUM (
    'DRAFT',
    'CONFIRMED'
);

CREATE TABLE public.events (
    id UUID UNIQUE NOT NULL DEFAULT uuid_generate_v4(),
    name VARCHAR(200) NOT NULL,
    picture VARCHAR(250) NOT NULL DEFAULT 'https://cdn.pixabay.com/photo/2018/01/05/00/20/test-image-3061864_1280.png',
    address VARCHAR(250) DEFAULT NULL,
    price   VARCHAR(50) DEFAULT NULL,
    max_participants INT DEFAULT NULL,
    date_of_event TIMESTAMP DEFAULT NULL,
    status public.event_status NOT NULL DEFAULT 'DRAFT',
    activity_id UUID DEFAULT NULL,
    created_by UUID NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT NOW(),
    confirmed_at TIMESTAMP DEFAULT NULL,
    group_id UUID DEFAULT NULL,

    CONSTRAINT fk_group_id FOREIGN KEY (group_id) REFERENCES public.groups(id),
    CONSTRAINT fk_created_by FOREIGN KEY (created_by) REFERENCES public.users(id),
    CONSTRAINT fk_activity_id FOREIGN KEY (activity_id) REFERENCES public.activities(id)
);

CREATE TYPE public.event_participations_enum as ENUM (
    'Interested',
    'Yes',
    'No'
);

CREATE TABLE public.event_participations(
    id UUID UNIQUE NOT NULL DEFAULT uuid_generate_v4(),
    user_id UUID NOT NULL,
    event_id UUID NOT NULL,
    participation_type public.event_participations_enum NOT NULL DEFAULT 'No',
    created_at TIMESTAMP NOT NULL DEFAULT NOW(),

    CONSTRAINT fk_user_id FOREIGN KEY (user_id) REFERENCES public.users(id),
    CONSTRAINT fk_event_id FOREIGN KEY (event_id) REFERENCES public.events(id)
);

CREATE TYPE public.survey_type AS ENUM (
    'DATE'
);

CREATE TYPE public.survey_value AS ENUM (
    'SEVEN_DAYS',
    'FOURTEEN_DAYS',
    'THIRTY_DAYS'
);

CREATE TABLE public.survey (
    id UUID UNIQUE NOT NULL DEFAULT uuid_generate_v4(),
    event_id UUID NOT NULL,
    survey_type public.survey_type NOT NULL,
    survey_value public.survey_value NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT NOW(),
    created_by UUID NOT NULL,

    CONSTRAINT fk_event_id FOREIGN KEY (event_id) REFERENCES public.events(id),
    CONSTRAINT fk_created_by FOREIGN KEY (created_by) REFERENCES public.users(id)
);

CREATE TABLE public.survey_answers (
    id UUID UNIQUE NOT NULL DEFAULT uuid_generate_v4(),
    survey_id UUID NOT NULL,
    answer TEXT NOT NULL,
    user_id UUID NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT NOW(),

    CONSTRAINT fk_survey_id FOREIGN KEY (survey_id) REFERENCES public.survey(id),
    CONSTRAINT fk_user_id FOREIGN KEY (user_id) REFERENCES public.users(id)
);

CREATE TABLE public.messages (
    id UUID UNIQUE NOT NULL DEFAULT uuid_generate_v4(),
    event_id UUID NOT NULL,
    user_id UUID NOT NULL,
    content TEXT,
    created_at TIMESTAMP NOT NULL DEFAULT NOW(),

    CONSTRAINT fk_event_id FOREIGN KEY (event_id) REFERENCES public.events(id),
    CONSTRAINT fk_user_id FOREIGN KEY (user_id) REFERENCES public.users(id)
);

CREATE TYPE public.founding_status AS ENUM (
    'ACTIVATED',
    'DEACTIVATED'
);

CREATE TABLE public.founding (
    id UUID UNIQUE NOT NULL DEFAULT uuid_generate_v4(),
    event_id UUID UNIQUE NOT NULL,
    price NUMERIC DEFAULT NULL,
    status public.founding_status NOT NULL DEFAULT 'DEACTIVATED',
    created_at TIMESTAMP NOT NULL DEFAULT NOW(),

    CONSTRAINT fk_event_id FOREIGN KEY (event_id) REFERENCES public.events(id)
);