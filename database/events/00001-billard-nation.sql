INSERT INTO public.events (
    name,
    picture,
    address,
    price,
    date_of_event,
    created_by
)
VALUES(
    'Billard a Nation', --name
    'http://www.shootagain.fr/wp-content/uploads/2014/07/04902.jpg', -- picture,
    '9 Cité Debergue, 75012 Paris', -- address
    '15', -- price
    '2023-03-18 18:00:00.08232+00', -- date of event
    '371ace0c-f5b4-4441-bad5-5313e7fc2868' -- created_by
);