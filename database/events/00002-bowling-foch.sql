INSERT INTO public.events (
    name,
    picture,
    address,
    price,
    date_of_event,
    created_by
)
VALUES(
    'Bownling a Etoile (Arc de Triomphe)', --name
    'https://10619-2.s.cdn12.com/rests/original/109_504818862.jpg', -- picture,
    'Face au 1 Avenue Foch, 75016 Paris', -- address
    '9', -- price
    '2023-03-25 18:00:00.08232+00', -- date of event
    '371ace0c-f5b4-4441-bad5-5313e7fc2868' -- created_by
);