\c dev

\i functions/create_or_get_users.sql
\i functions/check_token.sql
\i functions/create_event_for_groups.sql
\i functions/send_email_new_event.sql
\i functions/prepare_email_new_message.sql
\i functions/user_create_group.sql

TRUNCATE TABLE public.users CASCADE;
TRUNCATE TABLE public.events CASCADE;
TRUNCATE TABLE public.groups CASCADE;
TRUNCATE TABLE public.activities CASCADE;
TRUNCATE TABLE public.survey CASCADE;
TRUNCATE TABLE public.founding CASCADE;
TRUNCATE TABLE public.groups CASCADE;

ALTER TABLE public.messages DISABLE TRIGGER ALL;
ALTER TABLE public.events DISABLE TRIGGER ALL;

\i fixtures/activities.sql
\i fixtures/users.sql
\i fixtures/events.sql
\i fixtures/event_participations.sql
\i fixtures/messages.sql
\i fixtures/survey.sql
\i fixtures/founding.sql
\i fixtures/groups.sql

\i triggers/new_event.sql
\i triggers/new_messages.sql
