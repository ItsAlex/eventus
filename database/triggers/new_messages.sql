CREATE OR REPLACE TRIGGER send_email_new_message_trigger
    AFTER INSERT ON public.messages
    FOR EACH ROW
    EXECUTE FUNCTION public.prepare_email_new_message();
;