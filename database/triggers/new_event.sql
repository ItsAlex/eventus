CREATE OR REPLACE TRIGGER send_email_new_event_trigger
    AFTER INSERT ON public.events
    FOR EACH ROW
    EXECUTE FUNCTION public.send_email_new_event()
;