CREATE TABLE public.group_members (
    id UUID UNIQUE NOT NULL DEFAULT uuid_generate_v4(),
    group_id UUID NOT NULL,
    user_id UUID NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT NOW(),

    CONSTRAINT fk_group_id FOREIGN KEY (group_id) REFERENCES public.groups(id),
    CONSTRAINT fk_user_id FOREIGN KEY (user_id) REFERENCES public.users(id)
);