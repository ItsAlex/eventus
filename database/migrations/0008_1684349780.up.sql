CREATE TABLE public.survey_answers (
    id UUID UNIQUE NOT NULL DEFAULT uuid_generate_v4(),
    survey_id UUID NOT NULL,
    survey_question_id UUID NOT NULL,
    answer TEXT NOT NULL,
    user_id UUID NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT NOW(),

    CONSTRAINT fk_survey_id FOREIGN KEY (survey_id) REFERENCES public.survey(id),
    CONSTRAINT fk_survey_question_id FOREIGN KEY (survey_question_id) REFERENCES public.survey_questions(id),
    CONSTRAINT fk_user_id FOREIGN KEY (user_id) REFERENCES public.users(id)
);
