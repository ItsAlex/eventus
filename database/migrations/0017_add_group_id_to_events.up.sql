ALTER TABLE public.events
ADD COLUMN group_id UUID DEFAULT NULL;

ALTER TABLE public.events
ADD CONSTRAINT fk_group_id FOREIGN KEY (group_id) REFERENCES public.groups(id);

ALTER TABLE public.events
ADD CONSTRAINT fk_created_by FOREIGN KEY (created_by) REFERENCES public.users(id);

ALTER TABLE public.events
ADD CONSTRAINT fk_activity_id FOREIGN KEY (activity_id) REFERENCES public.activities(id);
