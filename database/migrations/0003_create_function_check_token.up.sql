CREATE OR REPLACE FUNCTION public.check_token(
    app_user_token UUID DEFAULT NULL
) RETURNS BOOLEAN AS $$
DECLARE
    _user_to_found UUID DEFAULT NULL;
BEGIN
    IF (app_user_token IS NULL) THEN
        RAISE EXCEPTION 'User not found';
    END IF;

    SELECT
        id
    INTO
        _user_to_found
    FROM
        public.users
    WHERE
        token=app_user_token;

    IF (_user_to_found IS NULL) THEN
        RAISE EXCEPTION 'User not found';
    END IF;

    RETURN TRUE;
END;
$$ LANGUAGE PLPGSQL SECURITY INVOKER;