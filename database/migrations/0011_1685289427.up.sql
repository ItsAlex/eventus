ALTER TABLE public.events
    ALTER COLUMN picture SET DEFAULT 'https://cdn.pixabay.com/photo/2018/01/05/00/20/test-image-3061864_1280.png';

ALTER TABLE public.events
    ALTER COLUMN address SET DEFAULT NULL;
ALTER TABLE public.events
    ALTER COLUMN price SET DEFAULT NULL;
ALTER TABLE public.events
    ALTER COLUMN date_of_event SET DEFAULT NULL;

ALTER TABLE public.events
    ALTER COLUMN date_of_event DROP NOT NULL;
ALTER TABLE public.events
    ALTER COLUMN price DROP NOT NULL;
ALTER TABLE public.events
    ALTER COLUMN address DROP NOT NULL;