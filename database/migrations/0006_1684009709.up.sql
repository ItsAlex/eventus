CREATE TABLE public.survey (
    id UUID UNIQUE NOT NULL DEFAULT uuid_generate_v4(),
    event_id UUID NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT NOW(),
    created_by UUID NOT NULL,

    CONSTRAINT fk_event_id FOREIGN KEY (event_id) REFERENCES public.events(id),
    CONSTRAINT fk_created_by FOREIGN KEY (created_by) REFERENCES public.users(id)
);

CREATE TABLE public.survey_questions (
    id UUID UNIQUE NOT NULL DEFAULT uuid_generate_v4(),
    survey_id UUID NOT NULL,
    question TEXT NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT NOW(),

    CONSTRAINT fk_survey_id FOREIGN KEY (survey_id) REFERENCES public.survey(id)
);
