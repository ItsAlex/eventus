CREATE TYPE public.founding_status AS ENUM (
    'ACTIVATED',
    'DEACTIVATED'
);

CREATE TABLE public.founding (
    id UUID UNIQUE NOT NULL DEFAULT uuid_generate_v4(),
    event_id UUID UNIQUE NOT NULL,
    price NUMERIC DEFAULT NULL,
    status public.founding_status NOT NULL DEFAULT 'DEACTIVATED',
    created_at TIMESTAMP NOT NULL DEFAULT NOW(),

    CONSTRAINT fk_event_id FOREIGN KEY (event_id) REFERENCES public.events(id)
);