CREATE TYPE public.group_frequency AS ENUM (
    'EVERY_WEEK',
    'EVERY_TWO_WEEKS'
);

CREATE TABLE public.groups (
    id UUID UNIQUE NOT NULL DEFAULT uuid_generate_v4(),
    name VARCHAR(250) NOT NULL,
    activity_id UUID NOT NULL,
    frequency public.group_frequency NOT NULL,
    created_by UUID NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT NOW(),

    CONSTRAINT fk_activity_id FOREIGN KEY (activity_id) REFERENCES public.activities(id),
    CONSTRAINT fk_created_by FOREIGN KEY (created_by) REFERENCES public.users(id)
);
