CREATE TABLE public.activities(
    id UUID UNIQUE NOT NULL DEFAULT uuid_generate_v4(),
    name VARCHAR(200) UNIQUE NOT NULL
);

INSERT INTO public.activities (
    name
)
VALUES(
    'Football 5-5'
),
(
    'Karaoké'
),
(
    'Tennis'
);


ALTER TABLE public.events
    ADD COLUMN activity_id UUID;