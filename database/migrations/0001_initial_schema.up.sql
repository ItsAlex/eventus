CREATE TABLE public.users (
    id UUID UNIQUE NOT NULL DEFAULT uuid_generate_v4(),
    email VARCHAR(200) UNIQUE NOT NULL,
    firstname VARCHAR(200) NOT NULL,
    lastname VARCHAR(200) DEFAULT '',
    token UUID UNIQUE NOT NULL DEFAULT uuid_generate_v4(),
    picture VARCHAR(250) DEFAULT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT NOW()
);

CREATE TABLE public.login (
    id UUID UNIQUE NOT NULL DEFAULT uuid_generate_v4(),
    user_id UUID NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT NOW()
);

CREATE TABLE public.events (
    id UUID UNIQUE NOT NULL DEFAULT uuid_generate_v4(),
    name VARCHAR(200) NOT NULL,
    picture VARCHAR(250) DEFAULT NULL,
    address VARCHAR(250) NOT NULL,
    price   VARCHAR(50) NOT NULL,
    max_participants INT DEFAULT NULL,
    date_of_event TIMESTAMP NOT NULL,
    created_by UUID NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT NOW()
);

CREATE TYPE public.event_participations_enum as ENUM (
    'Interested',
    'Yes',
    'No'
);

CREATE TABLE public.event_participations(
    id UUID UNIQUE NOT NULL DEFAULT uuid_generate_v4(),
    user_id UUID NOT NULL,
    event_id UUID NOT NULL,
    participation_type public.event_participations_enum NOT NULL DEFAULT 'No',
    created_at TIMESTAMP NOT NULL DEFAULT NOW(),

    CONSTRAINT fk_user_id FOREIGN KEY (user_id) REFERENCES public.users(id),
    CONSTRAINT fk_event_id FOREIGN KEY (event_id) REFERENCES public.events(id)
);