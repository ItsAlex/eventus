CREATE TYPE public.event_status AS ENUM (
    'DRAFT',
    'CONFIRMED'
);

ALTER TABLE public.events
    ADD COLUMN status public.event_status NOT NULL DEFAULT 'DRAFT';
