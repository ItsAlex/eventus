ALTER TABLE public.events
    ADD COLUMN confirmed_at TIMESTAMP DEFAULT NULL;
