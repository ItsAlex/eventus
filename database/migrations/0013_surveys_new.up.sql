ALTER TABLE public.survey_answers DROP CONSTRAINT fk_survey_question_id;
ALTER TABLE public.survey_answers DROP COLUMN survey_question_id;
DROP TABLE  IF EXISTS public.survey_questions CASCADE;

CREATE TYPE public.survey_type AS ENUM (
    'DATE'
);

CREATE TYPE public.survey_value AS ENUM (
    'SEVEN_DAYS',
    'FOURTEEN_DAYS',
    'THIRTY_DAYS'
);

ALTER TABLE public.survey ADD COLUMN survey_type public.survey_type NOT NULL;
ALTER TABLE public.survey ADD COLUMN survey_value public.survey_value NOT NULL;