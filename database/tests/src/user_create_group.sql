BEGIN;

    SELECT plan(4);

    SELECT lives_ok(
        $$
            SELECT public.user_create_group(
                user_id => '8e91753e-8c33-44fd-ada9-f4a2a62330ec',
                group_name => 'group name test',
                activity_id => 'd25b7e5b-8b23-490b-9140-55c766cd0001',
                frequency => 'EVERY_WEEK'
            )
        $$,
        'Should create new group and add creator as default member'
    );

    SELECT results_eq(
        $$
            SELECT
                name,
                activity_id,
                frequency,
                created_by
            FROM
                public.groups
            WHERE
                name = 'group name test'
            ORDER BY
                created_at DESC
            LIMIT 1
        $$,
        $$
            VALUES (
                'group name test'::VARCHAR,
                'd25b7e5b-8b23-490b-9140-55c766cd0001'::UUID,
                'EVERY_WEEK'::public.group_frequency,
                '8e91753e-8c33-44fd-ada9-f4a2a62330ec'::UUID
            )
        $$,
        'Should have created group with the right values'
    );

    SELECT is(
        (
            SELECT
                group_members.user_id
            FROM
                public.groups
            LEFT JOIN
                public.group_members
            ON
                groups.id = group_members.group_id
            WHERE
                groups.name = 'group name test'
            ORDER BY
                groups.created_at DESC
            LIMIT 1
        ),
        '8e91753e-8c33-44fd-ada9-f4a2a62330ec',
        'Should have created group with the right values'
    );

    SELECT is(
        (
            SELECT
                COUNT(group_members.user_id)
            FROM
                public.groups
            LEFT JOIN
                public.group_members
            ON
                groups.id = group_members.group_id
            WHERE
                groups.name = 'group name test'
            GROUP BY
                groups.created_at
            ORDER BY
                groups.created_at DESC
            LIMIT 1
        ),
        1::BIGINT,
        'Should have created group with the right values'
    );

SELECT * FROM finish();
ROLLBACK;