BEGIN;

    SELECT plan(5);

    SELECT results_eq(
        $$
            SELECT
                email,
                firstname,
                lastname,
                picture,
                created_at
            FROM
                public.create_or_get_user(
                    user_email => 'user0001@sakafet.fr',
                    user_firstname => 'user',
                    user_password => 'test'
                )
        $$,
        $$
            VALUES(
                'user0001@sakafet.fr'::VARCHAR, -- email
                'user'::VARCHAR, -- firstname
                'test'::VARCHAR, -- lastname
                'https://cdn.pixabay.com/photo/2022/11/29/00/17/witch-7623438_960_720.jpg'::VARCHAR, -- user picture
                '2023-04-28 20:36:09.271248+00'::TIMESTAMP
            )
        $$,
        'Assert that function create_or_get_user return the correct informations for existing user'
    );

    SELECT results_eq(
        $$
            SELECT
                email,
                firstname
            FROM
                public.create_or_get_user(
                    user_email => 'user-do-no-exist-yet@sakafet.fr',
                    user_firstname => 'test firstname'
                )
        $$,
        $$
            VALUES(
                'user-do-no-exist-yet@sakafet.fr'::VARCHAR, -- email
                'test firstname'::VARCHAR -- firstname
            )
        $$,
        'Assert that function create_or_get_user create new user'
    );

    SELECT throws_ok(
        $$
            SELECT
                email,
                firstname,
                lastname,
                picture,
                password
            FROM
                public.create_or_get_user(
                    user_email => 'user-do-no-exist@sakafet.fr',
                    user_firstname => 'test firstname',
                    user_password => 'test',
                    create_user_if_not_exist => FALSE
                )
        $$,
        'User not found',
        'Assert that function create_or_get_user return the correct informations for non-existing user'
    );

    SELECT throws_ok(
        $$
            SELECT
                email,
                firstname,
                lastname,
                picture,
                created_at
            FROM
                public.create_or_get_user(
                    user_email => 'user0001@sakafet.fr',
                    user_firstname => 'user'
                )
        $$,
        'Wrong password',
        'Assert that non existing user is not logged in if no password is sent'
    );

    SELECT throws_ok(
        $$
            SELECT
                email,
                firstname,
                lastname,
                picture,
                created_at
            FROM
                public.create_or_get_user(
                    user_email => 'user0001@sakafet.fr',
                    user_firstname => 'user',
                    user_password => 'incorrect password'
                )
        $$,
        'Wrong password',
        'Assert that non existing user is not logged in if password is wrong'
    );

SELECT * FROM finish();
ROLLBACK;