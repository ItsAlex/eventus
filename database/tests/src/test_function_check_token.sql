BEGIN;

    SELECT plan(3);

    SELECT is(
        (
            SELECT public.check_token(
                '200b583e-1544-4b45-99bb-1dcd9db979a4'::UUID
            )
        ),
        True,
        'Assert that user exist'
    );

    SELECT throws_ok(
        $$
            SELECT public.check_token()
        $$,
        'User not found',
        'Assert that if token is NULL, function raise an error'
    );

    SELECT throws_ok(
        $$
            SELECT public.check_token(
                '5c64e14e-3992-44cc-80fc-e223c26d53a2'::UUID
            )
        $$,
        'User not found',
        'Assert that if token is not related to any user, function raise an error'
    );

SELECT * FROM finish();
ROLLBACK;