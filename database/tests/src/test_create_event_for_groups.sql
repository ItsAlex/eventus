BEGIN;
    SELECT plan(3);

    DO $$ BEGIN
            INSERT INTO public.events (
                name,
                activity_id,
                group_id,
                created_by,
                date_of_event
            ) VALUES (
                'event already created',
                'd25b7e5b-8b23-490b-9140-55c766cd0003',
                '03e94668-0deb-47fa-b4dc-b44724eb0002',
                '8e91753e-8c33-44fd-ada9-f4a2a62330ec',
                NOW()
            );
    END $$;

    SELECT lives_ok(
        $$
            SELECT public.create_events_for_groups();
        $$,
        'Assert that function create_events_for_groups does not throw error'
    );

    SELECT results_eq(
        $$
            SELECT name, status, activity_id, created_by, group_id
            FROM public.events
            WHERE group_id = '03e94668-0deb-47fa-b4dc-b44724eb0001'
        $$,
        $$
            VALUES(
                'event 1'::VARCHAR,
                'DRAFT'::public.event_status,
                'd25b7e5b-8b23-490b-9140-55c766cd0001'::UUID,
                '8e91753e-8c33-44fd-ada9-f4a2a62330ec'::UUID,
                '03e94668-0deb-47fa-b4dc-b44724eb0001'::UUID
            )
        $$,
        'Assert that events was created with group_id 0001'
    );

    SELECT results_eq(
        $$
            SELECT name
            FROM public.events
            WHERE group_id = '03e94668-0deb-47fa-b4dc-b44724eb0002'
        $$,
        $$
            VALUES (
                'event already created'::VARCHAR
            )
        $$,
        'Assert that no events was created with group_id 0002 because previous event was too close'
    );

    SELECT * FROM finish();
ROLLBACK;