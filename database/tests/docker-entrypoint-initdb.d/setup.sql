CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
CREATE EXTENSION IF NOT EXISTS pgcrypto;

\i database/tables.sql

\i database/functions/create_or_get_users.sql
\i database/functions/check_token.sql
\i database/functions/create_event_for_groups.sql
\i database/functions/user_create_group.sql

\i database/fixtures/activities.sql
\i database/fixtures/users.sql
\i database/fixtures/events.sql
\i database/fixtures/event_participations.sql
\i database/fixtures/messages.sql
\i database/fixtures/survey.sql
\i database/fixtures/founding.sql
\i database/fixtures/groups.sql