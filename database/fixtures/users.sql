INSERT INTO public.users(
    id,
    email,
    firstname,
    lastname,
    picture,
    token,
    created_at,
    password
)
VALUES (
    '8e91753e-8c33-44fd-ada9-f4a2a62330ec'::UUID, -- id
    'user0001@sakafet.fr', -- email
    'user', -- firsatname
    'test', -- lastname
    'https://cdn.pixabay.com/photo/2022/11/29/00/17/witch-7623438_960_720.jpg', -- profil picture
    '200b583e-1544-4b45-99bb-1dcd9db979a4'::UUID, --token
    '2023-04-28 20:36:09.271248+00'::TIMESTAMP, -- created_at
    '$2a$06$dt9k7aTbGWFaTYMRhA/AIOD8KSWu8LbYVHgCq/zLEROY4S4iJASDG' -- password=test
);

INSERT INTO public.users(
    id,
    email,
    firstname,
    lastname,
    picture,
    token,
    created_at,
    password
)
VALUES (
    'b6543acd-7345-4e71-a55d-706e41af8ea0'::UUID, -- id
    'user0002@sakafet.fr', -- email
    'user2', -- firsatname
    'test2', -- lastname
    'https://cdn.pixabay.com/photo/2016/12/07/21/01/cartoon-1890438_960_720.jpg', -- profil picture
    '0f1d9e4e-2c0a-44f1-96c8-fa36bf0ae882'::UUID, -- token
    '2023-04-28 20:36:31.0208+00'::TIMESTAMP, -- created_at,
    '$2a$06$zgF2a8wg5LmeJ3SPri5H0OAfiZ4OgagS0lKYZUyWLJgsPZ/eFNkIC' -- password=test1
);

INSERT INTO public.users(
    id,
    email,
    firstname,
    lastname,
    picture,
    token,
    created_at,
    password
)
VALUES (
    '6eb3808a-a856-4deb-91a5-f1b7c95504f9'::UUID, -- id
    'user0003@sakafet.fr', -- email
    'user3', -- firsatname
    'test3', -- lastname
    'https://cdn.pixabay.com/photo/2016/12/13/16/17/dancer-1904467_960_720.png', -- profil picture
    '14742724-2220-4c38-912e-140354be18e9'::UUID, -- token
    '2023-06-28 20:36:31.0208+00'::TIMESTAMP, -- created_at,
    '$2a$06$zgF2a8wg5LmeJ3SPri5H0OAfiZ4OgagS0lKYZUyWLJgsPZ/eFNkIC' -- password=test1
);
