-- event 1

INSERT INTO public.event_participations (
    user_id,
    event_id,
    participation_type
)
VALUES (
    '8e91753e-8c33-44fd-ada9-f4a2a62330ec'::UUID, -- user_id
    '376ddd49-ffef-45c6-aadb-7a234f3be001'::UUID, -- event_id
    'Yes' -- participation_type
);

INSERT INTO public.event_participations (
    user_id,
    event_id,
    participation_type
)
VALUES (
    'b6543acd-7345-4e71-a55d-706e41af8ea0'::UUID, -- user_id
    '376ddd49-ffef-45c6-aadb-7a234f3be001'::UUID, -- event_id
    'Yes' -- participation_type
);

-- event 2

INSERT INTO public.event_participations (
    user_id,
    event_id,
    participation_type
)
VALUES (
    'b6543acd-7345-4e71-a55d-706e41af8ea0'::UUID, -- user_id
    '376ddd49-ffef-45c6-aadb-7a234f3be002'::UUID, -- event_id
    'Interested' -- participation_type
);

INSERT INTO public.event_participations (
    user_id,
    event_id,
    participation_type
)
VALUES (
    'b6543acd-7345-4e71-a55d-706e41af8ea0'::UUID, -- user_id
    '376ddd49-ffef-45c6-aadb-7a234f3be002'::UUID, -- event_id
    'No' -- participation_type
);

INSERT INTO public.event_participations (
    user_id,
    event_id,
    participation_type
)
VALUES (
         '6eb3808a-a856-4deb-91a5-f1b7c95504f9', -- user_id
         'a26ac11a-fd11-4833-a575-2fe4cf45d27c', -- event_id
         'Yes' -- participation_type
);

INSERT INTO public.event_participations (
    user_id,
    event_id,
    participation_type
)
VALUES (
         '8e91753e-8c33-44fd-ada9-f4a2a62330ec', -- user_id
         'a26ac11a-fd11-4833-a575-2fe4cf45d27c', -- event_id
         'Yes' -- participation_type
);

INSERT INTO public.event_participations (
    user_id,
    event_id,
    participation_type
)
VALUES (
         'b6543acd-7345-4e71-a55d-706e41af8ea0', -- user_id
         'a26ac11a-fd11-4833-a575-2fe4cf45d27c', -- event_id
         'Yes' -- participation_type
);
