INSERT INTO public.founding (
    event_id,
    price,
    status,
    created_at
) VALUES (
    '376ddd49-ffef-45c6-aadb-7a234f3be001'::UUID,
    10.00::NUMERIC,
    'DEACTIVATED',
    '2023-05-20 19:00:48.047228'
);

INSERT INTO public.founding (
    event_id,
    price,
    status,
    created_at
) VALUES (
    '376ddd49-ffef-45c6-aadb-7a234f3be003'::UUID,
    7.50::NUMERIC,
    'ACTIVATED',
    '2023-05-20 19:00:48.047228'
);