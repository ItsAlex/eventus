-- group 1

INSERT INTO public.groups (
    id,
    name,
    activity_id,
    frequency,
    created_at,
    created_by
) VALUES (
    '03e94668-0deb-47fa-b4dc-b44724eb0001',
    'group 1',
    'd25b7e5b-8b23-490b-9140-55c766cd0001',
    'EVERY_WEEK',
    '2023-06-14 19:00:48.047228',
    '8e91753e-8c33-44fd-ada9-f4a2a62330ec'
);

INSERT INTO public.group_members (
    user_id,
    group_id
) VALUES (
    '8e91753e-8c33-44fd-ada9-f4a2a62330ec', -- user001
    '03e94668-0deb-47fa-b4dc-b44724eb0001'
);

INSERT INTO public.group_members (
    user_id,
    group_id
) VALUES (
    'b6543acd-7345-4e71-a55d-706e41af8ea0', -- user002
    '03e94668-0deb-47fa-b4dc-b44724eb0001'
);

-- group 2

INSERT INTO public.groups (
    id,
    name,
    activity_id,
    frequency,
    created_at,
    created_by
) VALUES (
    '03e94668-0deb-47fa-b4dc-b44724eb0002',
    'group 2',
    'd25b7e5b-8b23-490b-9140-55c766cd0003',
    'EVERY_TWO_WEEKS',
    '2023-06-15 19:00:48.047228',
    '8e91753e-8c33-44fd-ada9-f4a2a62330ec'
);

INSERT INTO public.group_members (
    user_id,
    group_id
) VALUES (
    '8e91753e-8c33-44fd-ada9-f4a2a62330ec', -- user001
    '03e94668-0deb-47fa-b4dc-b44724eb0002'
);