
INSERT INTO public.messages (id,
                             event_id,
                             user_id,
                             content,
                             created_at)
VALUES ('1d9d47f4-115d-43fb-8800-e80068376e00',
        'a26ac11a-fd11-4833-a575-2fe4cf45d27c',
        '8e91753e-8c33-44fd-ada9-f4a2a62330ec',
        'Hello',
        '2023-07-08 09:59:06.161597');

INSERT INTO public.messages (id,
                             event_id,
                             user_id,
                             content,
                             created_at)
VALUES ('3ce2f910-8d56-4057-8b89-02a92fc3a330',
        'a26ac11a-fd11-4833-a575-2fe4cf45d27c',
        '8e91753e-8c33-44fd-ada9-f4a2a62330ec',
        'Ca va ?',
        '2023-07-08 09:59:16.161597');

INSERT INTO public.messages (id,
                             event_id,
                             user_id,
                             content,
                             created_at)
VALUES ('42d6854b-4545-4955-8a0f-105b504ef20f',
        'a26ac11a-fd11-4833-a575-2fe4cf45d27c',
        '8e91753e-8c33-44fd-ada9-f4a2a62330ec',
        'Prêt pour le tennis ?',
        '2023-07-08 10:00:06.161597');

INSERT INTO public.messages (id,
                             event_id,
                             user_id,
                             content,
                             created_at)
VALUES ('b0acf922-f23a-4411-9419-a070e27bf3a9',
        'a26ac11a-fd11-4833-a575-2fe4cf45d27c',
        '6eb3808a-a856-4deb-91a5-f1b7c95504f9',
        'Bien bien :)',
        '2023-07-08 10:00:16.161597');

INSERT INTO public.messages (id,
                             event_id,
                             user_id,
                             content,
                             created_at)
VALUES ('df784612-efc4-4b15-83f8-80c2a7317a39',
        'a26ac11a-fd11-4833-a575-2fe4cf45d27c',
        '6eb3808a-a856-4deb-91a5-f1b7c95504f9',
        'Tu vas prendre cher 🤡',
        '2023-07-08 10:01:06.161597');

INSERT INTO public.messages (id,
                             event_id,
                             user_id,
                             content,
                             created_at)
VALUES ('1f1f3dd0-7743-417c-b726-084ac5d8146d',
        'a26ac11a-fd11-4833-a575-2fe4cf45d27c',
        '6eb3808a-a856-4deb-91a5-f1b7c95504f9',
        'Challenge accepted',
        '2023-07-08 10:02:06.161597');

INSERT INTO public.messages (id,
                             event_id,
                             user_id,
                             content,
                             created_at)
VALUES ('eb8b8cc0-de8c-4ba9-b98b-84bbea0616c5',
        'a26ac11a-fd11-4833-a575-2fe4cf45d27c',
        'b6543acd-7345-4e71-a55d-706e41af8ea0',
        'Chaud aussi !',
        '2023-07-08 10:02:06.161597');

INSERT INTO public.messages (id,
                             event_id,
                             user_id,
                             content,
                             created_at)
VALUES ('ff7f1c25-90dd-4bc2-b1db-b80649ac849e',
        'a26ac11a-fd11-4833-a575-2fe4cf45d27c',
        '8e91753e-8c33-44fd-ada9-f4a2a62330ec',
        '😁',
        '2023-07-08 10:03:06.161597');

INSERT INTO public.messages (id,
                             event_id,
                             user_id,
                             content,
                             created_at)
VALUES ('7d8e02e3-b735-49e4-8cb1-10b415d76193',
        'a26ac11a-fd11-4833-a575-2fe4cf45d27c',
        '8e91753e-8c33-44fd-ada9-f4a2a62330ec',
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin id tincidunt purus. Praesent neque tortor, efficitur non ipsum sed, fringilla elementum purus. Cras hendrerit ipsum quis dolor blandit, ultrices volutpat felis condimentum. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Ut egestas tincidunt euismod. Nunc ultricies lorem elit, sed laoreet urna euismod vitae. Curabitur eu nisl est.',
        '2023-07-08 10:03:16.161597');

INSERT INTO public.messages (id,
                             event_id,
                             user_id,
                             content,
                             created_at)
VALUES ('84f6fa78-a06b-4e22-95b1-a45352b96382',
        'a26ac11a-fd11-4833-a575-2fe4cf45d27c',
        '6eb3808a-a856-4deb-91a5-f1b7c95504f9',
        'Je suis arrivé',
        '2023-07-08 10:03:26.161597');

INSERT INTO public.messages (id,
                             event_id,
                             user_id,
                             content,
                             created_at)
VALUES ('5e3c92f3-9b64-4816-9012-053472ea513b',
        'a26ac11a-fd11-4833-a575-2fe4cf45d27c',
        '8e91753e-8c33-44fd-ada9-f4a2a62330ec',
        'Ok',
        '2023-07-08 10:03:36.161597');
