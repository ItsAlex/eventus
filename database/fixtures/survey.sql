INSERT INTO public.survey (
    id,
    event_id,
    survey_type,
    survey_value,
    created_by,
    created_at
)
VALUES (
    '357f4f0b-b0c8-4f1e-b453-5e8b30240001'::UUID, -- id,
    '376ddd49-ffef-45c6-aadb-7a234f3be001'::UUID, -- event_id
    'DATE', -- survey_type
    'SEVEN_DAYS', -- survey_value
    '8e91753e-8c33-44fd-ada9-f4a2a62330ec'::UUID, -- created_by user_id 01
    '2023-04-30 20:36:09.271248+00'::TIMESTAMP -- created_at

);

INSERT INTO public.survey_answers(
    id,
    survey_id,
    answer,
    user_id,
    created_at
)
VALUES (
    '2572f40c-ae2c-436b-82db-de943a960001'::UUID, -- id,
    '357f4f0b-b0c8-4f1e-b453-5e8b30240001'::UUID, -- survey_id,
    'true,true,false,false,false,false,false', -- answer
    '8e91753e-8c33-44fd-ada9-f4a2a62330ec'::UUID, -- user_id 01
    '2023-05-01 20:36:09.271248+00'::TIMESTAMP -- created_at
);


INSERT INTO public.survey_answers(
    id,
    survey_id,
    answer,
    user_id,
    created_at
)
VALUES (
    '2572f40c-ae2c-436b-82db-de943a960002'::UUID, -- id,
    '357f4f0b-b0c8-4f1e-b453-5e8b30240001'::UUID, -- survey_id,
    'true,false,false,false,false,false,false', -- answer
    'b6543acd-7345-4e71-a55d-706e41af8ea0'::UUID, -- user_id 02
    '2023-05-01 20:36:09.271248+00'::TIMESTAMP -- created_at
);
