-- event 01

INSERT INTO public.events (
    id,
    name,
    picture,
    address,
    price,
    max_participants,
    date_of_event,
    status,
    created_by,
    created_at,
    confirmed_at
)
VALUES(
    '376ddd49-ffef-45c6-aadb-7a234f3be001'::UUID, -- id
    'Tennis a reuilly', --name
    'https://cdn.pixabay.com/photo/2016/05/09/11/09/tennis-1381230_1280.jpg', -- picture,
    '7 boulevard de reuilly', -- address
    '5.40', -- price
    '4', -- max participants,
    '2023-05-17 19:00:48.047228', -- date of event
    'CONFIRMED'::public.event_status, -- event status
    '8e91753e-8c33-44fd-ada9-f4a2a62330ec', -- created_by
    '2023-03-15 20:36:09.271248+00'::TIMESTAMP, -- created_at
    '2023-03-18 20:36:09.271248+00'::TIMESTAMP -- confirmed_at
);

-- event 02

INSERT INTO public.events (
    id,
    name,
    picture,
    address,
    price,
    max_participants,
    date_of_event,
    status,
    created_by,
    confirmed_at,
    created_at,
    activity_id
)
VALUES(
    '376ddd49-ffef-45c6-aadb-7a234f3be002'::UUID, -- id
    'Foot a Aubervillier', --name
    'https://cdn.pixabay.com/photo/2013/10/02/15/26/foosball-table-189846_1280.jpg', -- picture,
    '10 avenue Aubervillier', -- address
    '10.50', -- price
    '4', -- max participants,
    '2024-05-19 19:00:48.047228', -- date of event
    'CONFIRMED'::public.event_status, -- event status
    '8e91753e-8c33-44fd-ada9-f4a2a62330ec', -- created_by
    '2023-03-18 20:36:09.271248+00'::TIMESTAMP, -- confirmed at
    '2023-03-15 20:36:09.271248+00'::TIMESTAMP, -- created_at
    'd25b7e5b-8b23-490b-9140-55c766cd0001'::UUID -- activity_id
);

-- event 03

INSERT INTO public.events (
    id,
    name,
    picture,
    address,
    price,
    max_participants,
    date_of_event,
    status,
    created_by,
    confirmed_at,
    created_at,
    activity_id
)
VALUES (
    '376ddd49-ffef-45c6-aadb-7a234f3be003'::UUID, --id
    'Tennis a Montreuil', -- name
    'https://cdn.pixabay.com/photo/2018/01/05/00/20/test-image-3061864_1280.png', -- picture
    '10 rue de montreuil', -- address
    NULL, -- price
    NULL, -- max participants
    '2024-05-19 19:00:48.047228', -- date of event
    'DRAFT', -- event status
    '8e91753e-8c33-44fd-ada9-f4a2a62330ec', -- created_by
    '2023-03-18 20:36:09.271248+00'::TIMESTAMP, -- confirmed_at
    '2023-03-15 20:36:09.271248+00'::TIMESTAMP, -- created_at
    'd25b7e5b-8b23-490b-9140-55c766cd0003'::UUID -- activity_id
);

INSERT INTO public.events (
    id,
    name,
    picture,
    address,
    price,
    max_participants,
    date_of_event,
    status,
    created_by,
    confirmed_at,
    created_at,
    activity_id
)
VALUES (
    'a26ac11a-fd11-4833-a575-2fe4cf45d27c'::UUID, --id
    'Padel à UCPA 19e', -- name
    'https://cdn.pixabay.com/photo/2018/01/05/00/20/test-image-3061864_1280.png', -- picture
    'rosa parks', -- address
    NULL, -- price
    NULL, -- max participants
    '2024-06-19 19:00:48.047228', -- date of event
    'DRAFT', -- event status
    '6eb3808a-a856-4deb-91a5-f1b7c95504f9', -- created_by
    '2023-03-18 20:36:09.271248+00'::TIMESTAMP, -- confirmed_at
    '2023-03-15 20:36:09.271248+00'::TIMESTAMP, -- created_at
    'd25b7e5b-8b23-490b-9140-55c766cd0004'::UUID -- activity_id
);
