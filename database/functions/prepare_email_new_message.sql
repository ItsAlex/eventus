CREATE OR REPLACE FUNCTION public.prepare_email_new_message()
RETURNS TRIGGER AS $$
DECLARE
    _all_users_emails_to_send VARCHAR[];
    i_email VARCHAR;
    _sender_user_email VARCHAR;
BEGIN
    SELECT
        ARRAY_AGG(users.email)
    INTO
        _all_users_emails_to_send
    FROM
        public.event_participations
    JOIN
        public.users
    ON
        users.id = event_participations.user_id
    WHERE
        event_participations.event_id = new.event_id;

    SELECT
        email
    INTO
        _sender_user_email
    FROM
        public.users
    WHERE
        id = new.user_id;

    FOREACH i_email IN ARRAY _all_users_emails_to_send
    LOOP
        RAISE NOTICE 'sending email to %', i_email;
        IF i_email != _sender_user_email THEN
            PERFORM graphile_worker.add_job(
                'send_email_new_message',
                json_build_object(
                    'email', i_email,
                    'event_id', new.event_id
                )
            );
        END IF;
    END LOOP;
    RETURN (NEW);
END;
$$ LANGUAGE PLPGSQL SECURITY DEFINER;