CREATE OR REPLACE FUNCTION public.user_create_group(
    user_id UUID,
    group_name VARCHAR,
    activity_id UUID,
    frequency public.group_frequency
) RETURNS public.groups AS $$
DECLARE
    _group public.groups;
BEGIN
    INSERT INTO public.groups(
        name,
        activity_id,
        frequency,
        created_by
    ) VALUES (
        group_name,
        activity_id,
        frequency,
        user_id
    ) RETURNING * INTO _group;

    INSERT INTO public.group_members (
        group_id,
        user_id
    ) VALUES (
        _group.id,
        user_id
    );

    RETURN _group;
END
$$ LANGUAGE PLPGSQL SECURITY DEFINER;
