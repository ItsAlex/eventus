CREATE OR REPLACE FUNCTION public.send_email_new_event()
RETURNS TRIGGER AS $$
DECLARE
    _user_email_list VARCHAR[];
    _user_email VARCHAR;
BEGIN
    RAISE NOTICE 'event.created_by=%', new.created_by; -- will be necessary to send only to user friends;
    RAISE NOTICE 'event.id=%', new.id;

    SELECT
        ARRAY_AGG(email)
    INTO
        _user_email_list
    FROM
        public.users;

    FOREACH _user_email IN ARRAY _user_email_list
    LOOP
        PERFORM
            graphile_worker.add_job(
                'send_email_new_event',
                json_build_object(
                    'email', _user_email,
                    'event_id', new.id
                )
        );
    END LOOP;

    RETURN (new);
END;
$$ LANGUAGE PLPGSQL SECURITY INVOKER;