CREATE OR REPLACE FUNCTION public.create_events_for_groups()
RETURNS VOID AS $$
DECLARE
    _group public.groups;
    _event public.events;
    _group_frequency INT;
    _event_count BIGINT;
BEGIN

    FOR _group in (
        SELECT *
        FROM public.groups
    ) LOOP
        SELECT *
        INTO _event
        FROM public.events
        WHERE events.group_id = _group.id
        ORDER BY date_of_event DESC
        LIMIT 1;

        IF (_group.frequency = 'EVERY_WEEK') THEN
            _group_frequency = 7;
        ELSE
            _group_frequency = 14;
        END IF;

        IF (
            _event.date_of_event IS NULL
            OR
            (CURRENT_DATE - _event.date_of_event::DATE) > _group_frequency
        ) THEN
            SELECT COUNT(*) + 1
            INTO _event_count
            FROM public.events
            WHERE events.group_id = _group.id;

            INSERT INTO public.events (
                name,
                activity_id,
                group_id,
                created_by
            ) VALUES (
                'event ' || _event_count,
                _group.activity_id,
                _group.id,
                _group.created_by
            ) RETURNING * INTO _event;

            INSERT INTO public.survey (
                event_id,
                survey_type,
                survey_value,
                created_by
            ) VALUES (
                _event.id,
                'DATE'::public.survey_type,
                'FOURTEEN_DAYS'::public.survey_value,
                _group.created_by
            );
        END IF;
    END LOOP;
END;
$$ LANGUAGE PLPGSQL SECURITY DEFINER;