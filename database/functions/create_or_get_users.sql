CREATE OR REPLACE FUNCTION public.create_or_get_user(
    user_email      TEXT,
    user_firstname  TEXT,
    user_lastname   TEXT DEFAULT NULL,
    user_picture    TEXT DEFAULT NULL,
    user_password   TEXT DEFAULT NULL,
    create_user_if_not_exist BOOLEAN DEFAULT TRUE
) RETURNS public.users AS $$
DECLARE
    _user_to_found public.users DEFAULT NULL;
BEGIN
    SELECT
        id,
        email,
        firstname,
        lastname,
        token,
        picture,
        created_at,
        password
    INTO
        _user_to_found
    FROM
        public.users
    WHERE
        email = user_email;

    IF (
        _user_to_found IS NOT NULL
        AND
        _user_to_found.password IS NOT NULL
    ) THEN
        IF (
            user_password IS NULL
            OR
            REPLACE(_user_to_found.password, '$2b$', '$2a$') != crypt(user_password, REPLACE(_user_to_found.password, '$2b$', '$2a$'))
        )
        THEN
            RAISE EXCEPTION 'Wrong password';
        END IF;
    END IF;

    IF (_user_to_found IS NULL) THEN
        IF (create_user_if_not_exist IS FALSE) THEN
            RAISE EXCEPTION 'User not found';
        END IF;
        INSERT INTO
            public.users (
                email,
                firstname,
                lastname,
                picture,
                password
            )
        VALUES (
            user_email,
            user_firstname,
            user_lastname,
            user_picture,
            crypt(user_password, gen_salt('bf'))
        )
        RETURNING
            id,
            email,
            firstname,
            lastname,
            token,
            picture,
            created_at
        INTO
            _user_to_found;
    END IF;

    RETURN _user_to_found;
END;
$$ LANGUAGE PLPGSQL SECURITY INVOKER;